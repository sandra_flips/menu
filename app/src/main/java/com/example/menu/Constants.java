package com.example.menu;

public class Constants {
    public static final String TEXT_FUNCTION = "text";
    public static final String IMAGE_FUNCTION = "image";
    public static final String URL_FUNCTION = "url";
    public static final String TABLE_MENU_STAT = "menuStat";

}
