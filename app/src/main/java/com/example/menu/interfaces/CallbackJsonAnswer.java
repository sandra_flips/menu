package com.example.menu.interfaces;

import com.example.menu.pojo.Menu;

import java.util.ArrayList;

public interface CallbackJsonAnswer {

    void response(ArrayList<Menu> menuList);

    void failed();
}

