package com.example.menu.interfaces;

import android.graphics.Bitmap;

public interface CallbackImage {

    void response(Bitmap image);

    void failed();
}
