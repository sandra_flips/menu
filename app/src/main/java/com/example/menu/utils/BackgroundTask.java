package com.example.menu.utils;

import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.menu.interfaces.CallbackJsonAnswer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class BackgroundTask extends AsyncTask<String, Integer, JSONObject> {

    private CallbackJsonAnswer callback;

    public BackgroundTask(CallbackJsonAnswer callback) {
        this.callback = callback;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected JSONObject doInBackground(String... url) {
        try {
            return JsonParser.readJsonFromUrl(url[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) { // проверка на null у callback
        try {
            callback.response(JsonParser.parseJson(jsonObject));
        } catch (Exception e) {
            callback.failed();
        }
    }
}
