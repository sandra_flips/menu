package com.example.menu.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.example.menu.interfaces.CallbackImage;

import java.io.InputStream;

public class DownloadImage extends AsyncTask<String, Void, Bitmap> {

    private CallbackImage callback;

    public DownloadImage(CallbackImage callback) {
        this.callback = callback;
    }

    @Override
    protected Bitmap doInBackground(String... url) {
        try {
            return readImageFromUrl(url[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Bitmap image) {
        if (image != null)
            callback.response(image);
        else
            callback.failed();
    }


    public static Bitmap readImageFromUrl(String url) {
        Bitmap image = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            image = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return image;
    }
}
