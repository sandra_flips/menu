package com.example.menu.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.menu.pojo.Menu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class JsonParser {
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText;
            jsonText = readAll(rd);
            return new JSONObject(jsonText);
        }
    }

    public static ArrayList<Menu> parseJson(JSONObject json) throws JSONException {
        ArrayList<Menu> menu = new ArrayList<>();
        JSONArray list = json.getJSONArray("menu");
        for (int i = 0; i < list.length(); i++) {
            Menu item = new Menu();
            JSONObject name = list.getJSONObject(i);
            item.setName(name.getString("name"));
            JSONObject function = list.getJSONObject(i);
            item.setFunction(function.getString("function"));
            JSONObject param = list.getJSONObject(i);
            item.setParam(param.getString("param"));
            menu.add(item);
        }
        return menu;

    }

}
