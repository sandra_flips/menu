package com.example.menu.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class ParentFragment extends Fragment {
    public static final String ARG_PARAM1 = "param1";

    protected String mParam1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1, "No data");
        }
    }
}
