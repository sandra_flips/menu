package com.example.menu.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.menu.R;
import com.example.menu.interfaces.CallbackImage;
import com.example.menu.utils.DownloadImage;

public class ImageFragment extends ParentFragment implements CallbackImage {

    private ImageView imageJson;


    public ImageFragment() {
        // Required empty public constructor
    }


    public static ImageFragment newInstance(String param1) {
        ImageFragment fragment = new ImageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_image, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageJson = view.findViewById(R.id.imageView2);
        new DownloadImage(this).execute(mParam1);
    }

    @Override
    public void response(Bitmap image) {
        imageJson.setImageBitmap(image);
    }

    @Override
    public void failed() {
        Bitmap bDefault = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_android_black_24dp);
        imageJson.setImageBitmap(bDefault);
    }
}
