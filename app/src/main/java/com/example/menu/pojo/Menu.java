package com.example.menu.pojo;

public class Menu {

    private String name;
    private String function;
    private String param;

    public void setName(String name) {
        this.name = name;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getName() {
        return name;
    }

    public String getFunction() {
        return function;
    }

    public String getParam() {
        return param;
    }

}
