package com.example.menu.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.menu.Constants;
import com.example.menu.R;
import com.example.menu.database.DBUtils;
import com.example.menu.fragments.ImageFragment;
import com.example.menu.fragments.LinkFragment;
import com.example.menu.fragments.TextFragment;
import com.example.menu.interfaces.CallbackJsonAnswer;
import com.example.menu.utils.BackgroundTask;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements CallbackJsonAnswer, NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;// боковое меню дровер
    private Map<String, com.example.menu.pojo.Menu> mapMenu = new HashMap<String, com.example.menu.pojo.Menu>(); //интерфейс с меню
    private DBUtils dbUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar); // обращение к и установка тулбара
        setSupportActionBar(toolbar);

        dbUtils = new DBUtils(this);
        dbUtils.readAllClicks();
        drawer = findViewById(R.id.drawer_layout); // обращение к и установка бокового меню
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {
            // соединение функционала дровера и тулбара
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }


        };
        drawer.addDrawerListener(actionBarDrawerToggle);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionBarDrawerToggle.syncState();

        new BackgroundTask(this).execute("https://saven.co/sanya/menu.json"); // получение меню из jsonа в BackgroundTask который наследуется от AsyncTask
    }


    @Override
    public void response(ArrayList<com.example.menu.pojo.Menu> menuList) { // предопределение методов интерфейса при успешном получении меню из jsonа
        // список в котром хранится массив с данными из jsonа
        NavigationView navigationView = findViewById(R.id.nav_view);

        // контейнер для полученного меню
        Menu menuCustom = navigationView.getMenu(); // устанавливаем динамически меню

        for (com.example.menu.pojo.Menu menu : menuList) {
            menuCustom.add(menu.getName());
        }

        navigationView.setNavigationItemSelectedListener(this); // слушатель для меню

        for (com.example.menu.pojo.Menu menu : menuList) {
            mapMenu.put(menu.getName(), menu);
        }

        fragmentInitiate(menuList.get(0)); // установка первого фрагмента на домашнюю страничку
    }


    @Override
    public void failed() { //предопределение методов интерфейса при ошибки получении меню из jsonа (например если получили null)

        Toast.makeText(this, "Please, try again later!", Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {  // обработка нажатия


        String key = menuItem.getTitle().toString(); // ключ
        com.example.menu.pojo.Menu selectedMenu = mapMenu.get(key);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


        switch (selectedMenu.getFunction()) {
            case Constants.TEXT_FUNCTION:
                dbUtils.writeClicks(selectedMenu.getName());
                Fragment textFragment = TextFragment.newInstance(selectedMenu.getParam());
                fragmentTransaction.replace(R.id.nav_host_fragment, textFragment);
                break;
            case Constants.IMAGE_FUNCTION:
                dbUtils.writeClicks(selectedMenu.getName());
                Fragment imageFragment = ImageFragment.newInstance(selectedMenu.getParam());
                fragmentTransaction.replace(R.id.nav_host_fragment, imageFragment);
                break;
            case Constants.URL_FUNCTION:
                dbUtils.writeClicks(selectedMenu.getName());
                Fragment linkFragment = LinkFragment.newInstance(selectedMenu.getParam());
                fragmentTransaction.replace(R.id.nav_host_fragment, linkFragment);
                break;
            default:
                Toast.makeText(this, "Za molodih", Toast.LENGTH_LONG).show();
        }
        fragmentTransaction.addToBackStack(null);  // запоминает предидущие фрагменты нажатые
        fragmentTransaction.commit();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void fragmentInitiate(com.example.menu.pojo.Menu menu) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (menu.getFunction()) {
            case Constants.TEXT_FUNCTION:
                // фрагменты
                Fragment textFragment = TextFragment.newInstance(menu.getParam());
                fragmentTransaction.replace(R.id.nav_host_fragment, textFragment);
                break;
            case Constants.IMAGE_FUNCTION:

                Fragment imageFragment = ImageFragment.newInstance(menu.getParam());
                fragmentTransaction.replace(R.id.nav_host_fragment, imageFragment);

                break;
            case Constants.URL_FUNCTION:
                Fragment linkFragment = LinkFragment.newInstance(menu.getParam());
                fragmentTransaction.replace(R.id.nav_host_fragment, linkFragment);

                break;
            default:
                Toast.makeText(this, "Za molodih", Toast.LENGTH_LONG).show();

        }
        fragmentTransaction.addToBackStack(null);  // запоминает предидущие фрагменты нажатые
        fragmentTransaction.commit();
    }
}
