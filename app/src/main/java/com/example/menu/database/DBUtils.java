package com.example.menu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.example.menu.Constants;

public class DBUtils {

    // создаем объект для создания и управления версиями БД
    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private HandlerThread databaseThread;
    private final String LOG_TAG = "myLogs";
    private Handler databaseThreadHandler;
    private Cursor c;
    private ContentValues cv = new ContentValues();
    private Runnable readAllClicksRunnable = new Runnable() {
        @Override
        public void run() {

            c = db.query(Constants.TABLE_MENU_STAT, null, null, null, null, null, null);
            // ставим позицию курсора на первую строку выборки
            // если в выборке нет строк, вернется false
            if (c.moveToFirst()) {

                // определяем номера столбцов по имени в выборке
                int idColIndex = c.getColumnIndex("id");
                int nameColIndex = c.getColumnIndex("name");
                int countColIndex = c.getColumnIndex("count");
                do {
                    // получаем значения по номерам столбцов и пишем все в лог
                    Log.d(LOG_TAG,
                            "ID = " + c.getInt(idColIndex) +
                                    ", name = " + c.getString(nameColIndex) +
                                    ", count = " + c.getInt(countColIndex));
                    // переход на следующую строку
                    // а если следующей нет (текущая - последняя), то false - выходим из цикла
                } while (c.moveToNext());
            } else
                Log.d(LOG_TAG, "0 rows");

            assert c != null;
            c.close();
            dbHelper.close();
        }

    };

    public DBUtils(Context context) {
        dbHelper = new DBHelper(context);
        // подключаемся к БД
        db = dbHelper.getWritableDatabase();
        databaseThread = new HandlerThread("databaseThread");
        databaseThread.start();
        databaseThreadHandler = new Handler(databaseThread.getLooper());
    }

    public void readAllClicks() {
        databaseThreadHandler.post(readAllClicksRunnable);
    }

    public void writeClicks(String menuClick) {
        db = dbHelper.getWritableDatabase();
        int clickValue = 0;
        int idMenu = 0;
        c = db.query(Constants.TABLE_MENU_STAT, null, null, null, null, null, null);
        if (c.moveToFirst()) {

            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("name");
            int countColIndex = c.getColumnIndex("count");
            do {
                if (c.getString(nameColIndex).equals(menuClick)) {
                    clickValue = c.getInt(countColIndex);
                    idMenu = c.getInt(idColIndex);
                }
            } while (c.moveToNext());
        }
        cv.put("name", menuClick);
        cv.put("count", clickValue + 1);
        if (clickValue != 0) {
            db.update(Constants.TABLE_MENU_STAT, cv, "id = ?",
                    new String[]{String.valueOf(idMenu)});
        } else
            db.insert(Constants.TABLE_MENU_STAT, null, cv);
        dbHelper.close();

    }
}
